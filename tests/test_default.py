import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_sharelatex_containers(Command, Sudo):
    with Sudo():
        cmd = Command('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.split('\n')[1:]])
    assert names == ['sharelatex', 'sharelatex_mongo', 'sharelatex_redis', 'traefik_proxy']


def test_sharelatex_index(Command):
    # This tests that traefik forwards traffic to sharelatex
    # and that we can access the sharelatex index page
    cmd = Command('curl -H Host:sharelatex.docker.localhost -k -L https://localhost')
    assert '<title>Login - ShareLaTeX, Online LaTeX Editor</title>' in cmd.stdout
    assert 'ESS ShareLaTeX' in cmd.stdout
