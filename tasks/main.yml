---
- name: create ShareLaTeX directories
  file:
    path: "{{item.path}}"
    state: directory
    owner: "{{item.owner}}"
    group: "{{item.group}}"
    mode: 0755
  with_items:
    - path: "{{sharelatex_dir}}"
      owner: root
      group: root
    # sharelatex is run by user 33/33
    # but it doesn't touch the base data directory
    - path: "{{sharelatex_data}}"
      owner: root
      group: root
    # mongo is run by user 999/999
    - path: "{{sharelatex_mongo_data}}"
      owner: 999
      group: 999
    # redis is run by user 999/999
    - path: "{{sharelatex_redis_data}}"
      owner: 999
      group: 999

- name: create sharelatex.cnf file
  template:
    src: sharelatex.cnf.j2
    dest: "{{sharelatex_dir}}/sharelatex.cnf"
    owner: root
    group: root
    mode: 0640

- name: create sharelatex network
  docker_network:
    name: "{{sharelatex_network}}"
    state: present

- name: launch mongo container
  docker_container:
    name: sharelatex_mongo
    image: "mongo:{{sharelatex_mongo_tag}}"
    state: started
    restart_policy: always
    purge_networks: yes
    networks:
      - name: "{{sharelatex_network}}"
    exposed_ports:
      - 27017
    volumes:
      - "{{sharelatex_mongo_data}}:/data/db"

- name: launch redis container
  docker_container:
    name: sharelatex_redis
    image: "redis:{{sharelatex_redis_tag}}"
    state: started
    restart_policy: always
    purge_networks: yes
    networks:
      - name: "{{sharelatex_network}}"
    exposed_ports:
      - 6379
    volumes:
      - "{{sharelatex_redis_data}}:/data"

- name: launch sharelatex container
  docker_container:
    name: sharelatex
    image: "{{sharelatex_image}}:{{sharelatex_tag}}"
    state: started
    restart_policy: always
    purge_networks: yes
    networks:
      - name: "{{sharelatex_network}}"
      - name: "{{traefik_network}}"
    exposed_ports:
      - 80
    links:
      - sharelatex_redis
      - sharelatex_mongo
    volumes:
      - "{{sharelatex_data}}:/var/lib/sharelatex"
    privileged: yes
    env:
      SHARELATEX_MONGO_URL: mongodb://sharelatex_mongo/sharelatex
      SHARELATEX_REDIS_HOST: sharelatex_redis
      SHARELATEX_SITE_URL: "https://{{sharelatex_hostname}}"
    env_file: "{{sharelatex_dir}}/sharelatex.cnf"
    labels:
      traefik.enable: "true"
      traefik.backend: "sharelatex"
      traefik.port: "80"
      traefik.frontend.rule: "Host:{{sharelatex_hostname}}"
      traefik.docker.network: "{{traefik_network}}"
